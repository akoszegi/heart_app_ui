package com.weizheng.heartcarer1;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.SeriesSelection;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;

/**
 * Chart used to plot the curves using achart engine
 * @author yizhou
 *
 */
public class ECGChart {
    private XYMultipleSeriesRenderer renderer;
    private XYSeries series;
    private GraphicalView view;

    private int xAxis = 0;

    public ECGChart(Activity activity, double yMin, double yMax, String seriesTitle) {
        initRenderer(yMin, yMax);

        series = new XYSeries(seriesTitle);

        XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
        dataset.addSeries(series);

        // TODO: would getTimeChartView make it easier to display time properly?
        view = ChartFactory.getLineChartView(activity, dataset, renderer);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                @SuppressWarnings("unused")
                SeriesSelection seriesSelection = view.getCurrentSeriesAndPoint();
            }
        });
    }

    public void addToLayout(LinearLayout layout, LayoutParams layoutParams) {
        layout.addView(view, layoutParams);
        view.repaint();
    }

    private void initRenderer(double yMin, double yMax) {
        renderer = new XYMultipleSeriesRenderer();

        renderer.setAxisTitleTextSize(30);
        //renderer.setChartTitleTextSize(40);
        renderer.setLabelsTextSize(0);
        renderer.setLegendTextSize(30);
        //renderer.setMargins(new int[] {15, 35, 35, 10});//top,left,bottom,right
        renderer.setMargins(new int[] {0, 60, -55, 0});
        renderer.setZoomButtonsVisible(false);
        //renderer.setZoomEnabled(false,false);
        //renderer.setExternalZoomEnabled(true);
        renderer.setZoomEnabled(false);
        renderer.setPanEnabled(true, false);
        renderer.setPointSize(1);
        renderer.setShowGrid(true);
        renderer.setInScroll(false);
        renderer.setShowLegend(true);

        //Set Color
        renderer.setApplyBackgroundColor(true);
        renderer.setBackgroundColor(Global.color_Pink);
        //renderer.setMarginsColor(Global.color_Pink);
        renderer.setMarginsColor(Global.color_White);

        renderer.setGridColor(Global.color_Red);
        renderer.setAxesColor(Global.color_Red);
        renderer.setLabelsColor(Global.color_Black);
        renderer.setXLabelsColor(Global.color_Black);
        renderer.setYLabelsColor(0, Global.color_Black);

        // TODO: Test y range!!!
        //Set X,Y Axis Range
        renderer.setXTitle("Time: ms");
        renderer.setXAxisMin(0);
        renderer.setXAxisMax(Global.xAxis_Max);
        renderer.setXLabelsPadding(40);
        //renderer.setLegendHeight(-5);
        renderer.setYTitle("Voltage: V");
        renderer.setYAxisMin(yMin);
        renderer.setYAxisMax(yMax);
        renderer.setYLabels(10);
        renderer.setYLabelsPadding(30); // TODO: Test!
        renderer.setShowLabels(true);

        XYSeriesRenderer seriesRenderer = new XYSeriesRenderer();
        seriesRenderer.setPointStyle(PointStyle.CIRCLE);
        seriesRenderer.setFillPoints(true);
        seriesRenderer.setColor(Global.color_Black);
        renderer.addSeriesRenderer(seriesRenderer);
    }

    public void appendPoint(double y) {
        if (xAxis == Global.xAxis_Total_Max)
            clearPlot();

        series.add(xAxis++, y);
        view.repaint();

        if (xAxis > Global.xAxis_Max)
            restRangePlot();
    }

    public void appendPointWithoutPaint(double y) {
        Log.d("ECGChart", "apprendPOintWIthoutPain - pt = "+y);

        if (xAxis == Global.xAxis_Total_Max)
            clearPlot();            // TODO: Don't completely reset graph, try scrolling?

        series.add(xAxis++, y);

        if (xAxis > Global.xAxis_Max)
            restRangePlot();
    }

    public void repaint() {
        view.repaint();
    }

    public void clearPlot() {
        series.clear();
        xAxis = 0;

        renderer.setXAxisMin(0);
        renderer.setXAxisMax(Global.xAxis_Max);
    }

    private void restRangePlot() {
        renderer.setXAxisMin(xAxis - Global.xAxis_Max);
        renderer.setXAxisMax(xAxis);
    }
}
