package com.weizheng.heartcarer1;

import java.util.ArrayList;
import java.util.UUID;

import com.weizheng.heartcarer1.R;

import android.app.Fragment;
import android.app.ListActivity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

/**
 * Provide interface for starting and stopping Bluetooth scans for devices
 *
 */

public class BleDeviceFinder {
	private final String TAG = "BleDeviceFinder";
	private Context context = null;
	private UUID[] uuids;
	private String[] deviceNames;

	private final int SCAN_PERIOD = 10000;//10s
	private boolean ifScanning = false;
    private Handler mHandler = new Handler();
	BleDeviceFinderInterface deviceFinderCallback;

	public interface BleDeviceFinderInterface {
		public void deviceFound();
		public void scanFinished(boolean deviceFound);
	}

	public BleDeviceFinder(String deviceBleAddress, Context c) {
		context = c;
		scanLeDevice();
	}

	public BleDeviceFinder(Context c, Fragment f) {
		context = c;
		deviceFinderCallback = (BleDeviceFinderInterface) f;
	}

	// Use service uuids and device names to find and connect to devices because
	// we're using a generic Heart Rate Service (many devices have this service)
	public void findDevices(UUID[] deviceBleAddress, String[] deviceNames) {
		Log.d(TAG, "find devices");
		uuids = deviceBleAddress;
		this.deviceNames = deviceNames;

		scanLeDevice();
	}

	//Stop scanning BLE devices
	private void stopScan() {
		Log.d(TAG, "stopScan");
		ifScanning = false;
		deviceFinderCallback.scanFinished(false);

		MainActivity.mBluetoothAdapter.stopLeScan(mLeScanCallback);

	}


	//Scan BLE devices
	private void scanLeDevice() {
		Log.d(TAG, "scanLeDevices");
		ifScanning = true;
		//MainActivity.mBluetoothAdapter.startLeScan(uuids, mLeScanCallback);
		MainActivity.mBluetoothAdapter.startLeScan(mLeScanCallback);

		// Stops scanning after a pre-defined scan period.
		mHandler.postDelayed(new Runnable() {
			public void run() {
				stopScan();
			}
		}, SCAN_PERIOD);
    }
	private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {
		//Called when BLE device found during scanning
		BluetoothDevice dev;

		public void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord) {
			dev = device;

			Log.d(TAG, "Le Scan Callback - found device " + device.getName() + "\n" +
				" UUIDs: " + device.getUuids());




			for (String d : deviceNames) {
				if (d.equals(device.getName())) {
					//if(d.equals(getResources().getString(R.string.app_name_title))) {

					Log.d(TAG, "found a device we want to connect to");
					// Device is in the list of devices to connect to so connect
					if(ifScanning) stopScan();
					HrmFragment.ifAutoConnect = true;
					BleService.mDevice = device;

					// TODO: send message to HrmFragment
					deviceFinderCallback.deviceFound();

				}


			}


		}

		Runnable scanCallbackRunnable = new Runnable() {
			@Override
			public void run() {
				BleService.mDevice = dev;
				if(ifScanning) stopScan();
				HrmFragment.ifAutoConnect = true;
			}
		};
	};
	//Customized ListAdapter
/*	private class LeDeviceListAdapter extends BaseAdapter {
		private ArrayList<BluetoothDevice> mLeDevices;
  		private LayoutInflater mInflator;
  		
  		public LeDeviceListAdapter() {
  			super();
  			mLeDevices = new ArrayList<BluetoothDevice>();
  			mInflator = getLayoutInflater();
  		}
  		public void addDevice(BluetoothDevice device) {
  			if(!mLeDevices.contains(device)) mLeDevices.add(device);
  		}
  		public void clear() {
  			mLeDevices.clear();
  		}
		public int getCount() {
			return mLeDevices.size();
		}
		public BluetoothDevice getItem(int position) {
			return mLeDevices.get(position);
		}
		public long getItemId(int position) {
			return position;
		}
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder viewHolder;
			if (convertView == null) {
				convertView = mInflator.inflate(R.layout.datamanagelist_item, null);
				viewHolder = new ViewHolder();
				viewHolder.image = (ImageView)convertView.findViewById(R.id.imageView1);
				viewHolder.deviceName = (TextView)convertView.findViewById(R.id.textView1);
				viewHolder.deviceAddress = (TextView)convertView.findViewById(R.id.textView2);
  				convertView.setTag(viewHolder);
			}else{
				viewHolder=(ViewHolder)convertView.getTag();
			}
			BluetoothDevice device = mLeDevices.get(position);
			final String deviceName = device.getName();
			if (deviceName != null && deviceName.length() > 0){
				viewHolder.deviceName.setText(deviceName);
				if(deviceName.equals(getResources().getString(R.string.app_name_title)))
					viewHolder.image.setBackgroundResource(R.drawable.main_heart_beat_64);
				else
					viewHolder.image.setBackgroundResource(R.drawable.unknown_64);
				
			}else{
				viewHolder.deviceName.setText("Unknown Device");
				viewHolder.image.setBackgroundResource(R.drawable.unknown_64);
			}
			viewHolder.deviceAddress.setText(device.getAddress());
			return convertView;
		}
		private class ViewHolder{
  			private ImageView image;
  			private TextView deviceName;
  			private TextView deviceAddress;
  		}
	}	*/
}