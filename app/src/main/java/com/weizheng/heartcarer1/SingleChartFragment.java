package com.weizheng.heartcarer1;

import com.weizheng.ECG.DataFilter;

import android.app.Fragment;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;

/**
 * Single Chart Fragment
 * @author yizhou
 *
 */
public class SingleChartFragment extends Fragment {
    private View view;
    private ECGChart ecgChart;

    private DataFilter dataFilter = new DataFilter();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.single_chart_fragment, container, false);

        ecgChart = new ECGChart(getActivity(), Global.getYAxisMin(), Global.getYAxisMax(), " ");
        // Series title was: getResources().getString(R.string.ecgsignal)
         ecgChart.addToLayout((LinearLayout) view.findViewById(R.id.single_chart),
                new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));

        return view;
    }

    // TODO: Update switch with Message_Types enum values
    public void handleHrmFragmentMes(Message msg) {
        // TODO: Handle Channel 2 selection
        // Case = 0 handles channel 1, Case = 0 handles both
        switch (msg.what) {
        case 0:
            int[] multiValue = msg.getData().getIntArray("multiValue");
            int length = multiValue.length / 2;

            Log.d("SingleChartFrag", "received data to plot: " + multiValue.length);

            // QUES: Why do we only look at half the data? One channel?
            // QUES: Added brackets for loop
            for (int i = 0; i < length; i++){
                // TODO: channel is only 0 or 1, so only impacts whether or not first index is used
                // QUES: What data is in odd indeces and why don't we use that?
                // TODO: Check what happens if Channel_select = 1?
                ecgChart.appendPointWithoutPaint(dataFilter.dataConvert(multiValue[i * 2 + Global.Channel_selection]) / 2);

            }
                ecgChart.repaint(); // should this be in loop??
            break;
            case 1:


                break;

        case 2:
            ecgChart.clearPlot();
            break;
        default:
            break;
        }
    }
}
