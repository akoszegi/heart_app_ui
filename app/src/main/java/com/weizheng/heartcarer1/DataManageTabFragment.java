package com.weizheng.heartcarer1;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;


/**
 * Created by AlexKoszegi on 2017-07-19.
 */

public class DataManageTabFragment extends Fragment {
    // Store reference to a Fragment Activity so we can access FragmentSupportManager
    private FragmentActivity fragActivityContext;
    FragmentTabHost tabHost;

    String TAG = "DataManageTabFragment";

    @Override
    public void onAttach(Activity activity) {
        Log.d(TAG, "onAttach");
        fragActivityContext = (FragmentActivity)activity;
        super.onAttach(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");
        View v = inflater.inflate(R.layout.data_manage_tab_fragment, container, false);

       /* TabHost tabHostWidget = (TabHost)v.findViewById(android.R.id.tabhost);
        tabHostWidget.setup();

        TabHost.TabSpec quickSpec = tabHostWidget.newTabSpec("Quick");
        quickSpec.setContent(R.id.quick_tab);
        quickSpec.setIndicator("Quick");
        tabHostWidget.addTab(quickSpec);

        TabHost.TabSpec longtermSpec = tabHostWidget.newTabSpec("Longterm");
        longtermSpec.setContent(R.id.long_term_tab);
        longtermSpec.setIndicator("Longterm");
        tabHostWidget.addTab(longtermSpec); */


        // Instantiate Tab Bar
        tabHost = new FragmentTabHost(getActivity());
        tabHost.setup(getActivity(), fragActivityContext.getSupportFragmentManager(), R.id.tab_host);

        // Add tabs to tab bar

        tabHost.addTab(tabHost.newTabSpec("Quick").setIndicator("Quick"),
                QuickTestsTabContent.class, null);
        tabHost.addTab(tabHost.newTabSpec("Long").setIndicator("Long Term"),
                LongTestsTabContent.class, null);

        return tabHost;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        tabHost = null;
    }
}
