package com.weizheng.heartcarer1;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

import com.weizheng.ECG.AveCC;
import com.weizheng.ECG.HR_FFT;
import com.weizheng.ECG.HR_detect;
import com.weizheng.ECG.MADetect;
import com.weizheng.ECG.MADetect1;
import com.weizheng.heartcarer1.R;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.Vibrator;
import android.util.Log;

//This Service runs after logging in and ends when exiting app
@SuppressLint("HandlerLeak")
public class BleService extends Service {
	//0-Not Connected; 1-Connected; 2-Connecting
	public static int ConState;
	public static final int ConState_NotConnected = 0;
	public static final int ConState_Connected = 1;
	public static final int ConState_Connecting = 2;
	public static boolean enableNoti;
	public static boolean ifDraw;
	public static BluetoothDevice mDevice;
	private HR_FFT hr = new HR_FFT();
	private HR_detect hrd = new HR_detect();
	private final String TAG = "BleService";
	private Messenger ActivityMessenger;
	private BluetoothGatt mBluetoothGatt;
	private BluetoothGattCharacteristic characteristic;
	private BluetoothGattDescriptor descriptor;
	private boolean ifSavingToFile;
	private boolean flagBuffer_1 = true;
	private boolean flag_buffer_2 = false;
	private final int buf_length = 3750;
	private byte[] buffer_1 = new byte[buf_length];
	private byte[] buffer_2 = new byte[buf_length];
	private double[] buffer_HR = new double[buf_length];
	private List<Double> buffer_HR_list = new ArrayList<Double>();
	private int pointer_HR = 0;
	private int pointerBuf = 0;
	private boolean flagSaveBufInit = false;
	private int pointerBufInit;
	private int pointerBufEnd;
	private OutputStream output;
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault());
	private String saveFileName;
	private Timer timerSavingFile = null;
	private TimerTask taskTimerSavingFile = null;
	private boolean flagTimerSavingFileFirst = true;
	private boolean ifHbNormal = true;
	private Handler mHandler = new Handler();
	private Vibrator vibrator;
	private final int VibrateLength = 300; //unit: ms
	private NotificationManager mNotiManager;
	private int i = 0;
	File outputFile = new File(Global.RootPath+"/1.txt");  
    FileOutputStream outputFileStream = null;

	//Compressed Sensing
	private int csCounter;
	private ArrayList<byte[]> csByteArray = new ArrayList<byte[]>();
	private boolean iffirst=true; //if the data is the first one of once test, 1 for true



	public void onCreate() {
		Log.i(TAG, "onCreate()");
		
		initStatic();
		registerReceiver(wifiReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
		vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
		mNotiManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
	}
	//Initiate static variables
	private void initStatic() {
		ConState = ConState_NotConnected;
		enableNoti = true;
		ifDraw = Global.ifCsMode ? false : true;
		Global.ifSaving = false;
		mDevice = null;
	}
	//Called by the system every time a client starts the service by calling "startService"
	@Override
	public int onStartCommand (Intent intent, int flags, int startId){
		Log.i(TAG, "onStartCommand()");
		Log.v("startId", String.valueOf(startId));
		
		if(startId==1) StartForeground();
	    return super.onStartCommand(intent, flags, startId);
	}
	//Show foreground notification
	private void StartForeground() {
		Notification noti = new Notification.Builder(BleService.this)
		    .setContentIntent(PendingIntent.getActivity(BleService.this, 0,
		    		Global.defaultIntent(BleService.this), 0))
            .setContentTitle(getResources().getString(R.string.app_name))
            .setContentText(getResources().getString(R.string.app_name) + getResources().getString(R.string.ble_run))
            .setSmallIcon(R.drawable.main_heart_beat_64)
            .build();
		startForeground(1, noti);
	}
	//Return the communication channel to the service
	@Override
	public IBinder onBind(Intent intent) {
		Log.i(TAG, "onBind()");
		return mMessenger.getBinder();
	}
	//When wifi is connected, upload saved files
	private BroadcastReceiver wifiReceiver = new BroadcastReceiver(){
		public void onReceive(Context arg0, Intent intent) {
			NetworkInfo networkInfo = ((ConnectivityManager)getSystemService
					(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
			if(networkInfo != null && networkInfo.getType() == ConnectivityManager.TYPE_WIFI
					&& Global.ifRegUser && !Global.ifUploading){
				startService(new Intent(BleService.this, UpdataService.class));
			}
		}
	};
	//Disconnect BLE connection
	private void disconnect() {
    	mBluetoothGatt.disconnect();
	}

	//Handler which handles incoming Message from UI Thread
	private class IncomingHandler extends Handler {
		public void handleMessage(Message msg) {
			int i = msg.what;
			if(i==0){
				//Register
				ActivityMessenger = msg.replyTo;
				sendVoidToAM(6, "");
			}else if(i==1){
				//Connect
				// TODO: Scan again
				if (mDevice == null) {

				}

				try {  
		            outputFileStream = new FileOutputStream(outputFile);  
		        } catch (FileNotFoundException e) {  
		            e.printStackTrace();  
		        }  
				mBluetoothGatt = mDevice.connectGatt(BleService.this, false, mGattCallback);
				System.out.print("connect");		
			}else if(i==2){
				//Disconnect
	            try {
					outputFileStream.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}  
				disconnect();
				
			}else if(i==3){
				//SetNoti
				setNoti();
			}else if(i==4){
				//Button - SaveToFile
				if(ConState!=ConState_Connected || enableNoti){
					toastMakeText("Please start " + getResources().getString(R.string.global_sensor) + "!");
	    			return;
	    		}else{
	    			if(Global.ifSaving){
	    				stopSavingFinal(true);
	    			}else{
	    				String filename = startSaving(iffirst);
	    				Global.ifSaving = true;
	    				toastMakeText("Start saving!");
						sendVoidToAM(3, filename);
						if(!Global.ifCsMode) startTimerSavingFile();
	    			}
	    		}
			} else if (i == 5) {
				//Quick_check
				if(ConState!=ConState_Connected || enableNoti){
					toastMakeText("Please start " + getResources().getString(R.string.global_sensor) + "!");
	    			return;
	    		}else{
	    			if(Global.ifSaving){
	    				stopSavingFinal(false);
	    			}else{
	    				String filename = startSaving(iffirst);
	    				Global.ifSaving = true;
	    				toastMakeText("Start saving!");
						sendVoidToAM(3, filename);
						if(!Global.ifCsMode) startTimerSavingFile();
	    			}
	    		}
			}
		}
	}
	//Local Messenger used to talk to ActivityMessenger, Message received by IncomingHandler
	final Messenger mMessenger = new Messenger(new IncomingHandler());

	private void sendVoidToAM(int i, String s){
        Log.d(TAG, "sendVoidToAM, i = " + i + ", string = " + s);
		if(Global.ifHrmFragmentAlive){
			try {
				Message message = new Message();
				message.what = i;
				Bundle bundle = new Bundle();
				bundle.putString("filename", s);
				message.setData(bundle);
				ActivityMessenger.send(message);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	//Start saving ECG data to file
	private String startSaving(boolean iffirst){
		Log.v(TAG, "startSaving()");
		//int ifAutoSave=Global.ifAutoSave ? 1:0;
		String testtime=iffirst ? sdf.format(new Date(System.currentTimeMillis())): Global.testtime;
		if(iffirst) Global.testtime=testtime;
		String iffirst_int = iffirst ? "1":"0";
		saveFileName = sdf.format(new Date(System.currentTimeMillis())) +  
				(Global.ifCsMode ? "1" : "0") + testtime + iffirst_int + ".bin";
		try{
			output = new BufferedOutputStream(new FileOutputStream(Global.cachePath + 
					"/" + saveFileName));
		}catch (Exception e){
			e.printStackTrace();
			toastMakeText("Error: Start saving!");
			stopSavingFinal(true);
			return "";
		}
		if(Global.ifCsMode){
			csCounter = 0;
		}else{
			flagSaveBufInit = true;
			pointerBufInit = pointerBuf;
		}
        Log.d(TAG, "saveFileName: " + saveFileName );
		return saveFileName;
	}
	//Stop saving ECG data to file
	private void stopSaving(boolean flg){
		Log.v(TAG, "stopSaving()");
		
		if(Global.ifCsMode){
			if(ifSavingToFile) return;
		}else{
			pointerBufEnd = pointerBuf;
			byte[] tempByte = new byte[flagSaveBufInit ? 
					pointerBufEnd - pointerBufInit : pointerBufEnd];
			System.arraycopy(flagBuffer_1 ? buffer_1 : buffer_2,
					flagSaveBufInit ? pointerBufInit : 0, tempByte, 0,
					flagSaveBufInit ? pointerBufEnd-pointerBufInit : pointerBufEnd);
			saveToFile(tempByte);
		}
		try{
			output.close();
			String oldPath = Global.cachePath + "/" + saveFileName;
			if(Global.ifCsMode){
				csByteArray.clear();
				if(csCounter==0){
					toastMakeText("Less than 4 seconds, no data saved!");
					new File(oldPath).delete();
					return;
				}
				String oldPathCs = oldPath;
				//saveFileName = saveFileName.replace(".", csCounter + ".");
				csCounter = 0;
				oldPath = Global.cachePath + "/" + saveFileName;
				new File(oldPathCs).renameTo(new File(oldPath));
			}
			String newPath;
			if (flg) {
				newPath = Global.savedPath + "/" + saveFileName;
				new File(oldPath).renameTo(new File(newPath));
			} else {
				newPath = Global.quickcheckpath + "/" + saveFileName;
				File file = new File(oldPath);
				File file_new = new File(newPath);
				FileOutputStream out = new FileOutputStream(file_new);
		        byte[] tmp = new byte[5];
				try {
					FileInputStream in = new FileInputStream(file);
					while (in.read(tmp) != -1) {
						out.write(tmp[3]);
						out.write(tmp[4]);
					}
					in.close();
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}

			}

		}catch (Exception e){
		    e.printStackTrace();
		    toastMakeText("Error: Stop saving!");
		}
		toastMakeText(saveFileName + " saved!");
		if((!Global.ifUploading) && Global.isWifiConn(BleService.this) && (flg)){
			startService(new Intent(BleService.this, UpdataService.class));
		}
		System.gc();
	}
	//Stop saving to file thoroughly
	private final void stopSavingFinal(boolean flg){
		if(Global.ifSaving){
			if(!Global.ifCsMode) cancelTimerSavingFile();
			Global.ifSaving = false;
			stopSaving(flg);
			iffirst=true;
		}
		sendVoidToAM(4, "");
	}
	//BLE Callback
	private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
		//Called when connection state changes
		public void onConnectionStateChange (BluetoothGatt gatt, int status, int newState){
			if (newState == BluetoothProfile.STATE_CONNECTED) {
				ConState = ConState_Connected;
				sendVoidToAM(1, "");
				mBluetoothGatt.discoverServices();
				vibrator.vibrate(VibrateLength);
			}else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
				ConState = ConState_NotConnected;
				enableNoti = true;
				sendVoidToAM(2, "");
				//Re-initiate mBluetoothGatt
				if(mBluetoothGatt != null){
		        	mBluetoothGatt.close();
		        	mBluetoothGatt = null;
		        }
				stopSavingFinal(true);
				vibrator.vibrate(VibrateLength);
			}
		}
		//Called when BLE services are discovered
		public void onServicesDiscovered(BluetoothGatt gatt, int status) {
			Log.v(TAG, "onServicesDiscovered()");
			if (status==BluetoothGatt.GATT_SUCCESS) {
				initNoti();
				setNoti();
				//toastMakeText("SERVICE DISCOVERD!!!");
			}else{
				toastMakeText("Error: Discover Services!");
			}
		}
		//Called when notification received
		@SuppressWarnings("unchecked")
		public void onCharacteristicChanged (BluetoothGatt gatt,
				BluetoothGattCharacteristic characteristic){
			//Log.v(TAG, "onCharacteristicChanged()");
			byte[] notiValue = characteristic.getValue();

			Log.d(TAG, "characteristic value: " + getHexString(notiValue) + "\n"
				+ "value length: " + notiValue.length);

			try {
				outputFileStream.write(notiValue);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			/* Extract 3750 bytes from notification value
				First fill buffer_1. If there are more than
				3750 bytes start filling buffer_2
			*/
			for (int i = 0 ; i < notiValue.length ; i++ ) {
				if(flagBuffer_1){
					buffer_1[pointerBuf] = notiValue[i];
					if(pointerBuf == buf_length-1){
						pointerBuf = 0;
						flagBuffer_1 = false;
						flag_buffer_2 = true;
						if(Global.ifSaving) saveBufData(1);
					}else pointerBuf++;
				} else if(flag_buffer_2) {
					buffer_2[pointerBuf] = notiValue[i];
					if(pointerBuf==(buf_length-1)){
						pointerBuf = 0;
						flag_buffer_2 = false;
						flagBuffer_1 = true;
						if(Global.ifSaving) saveBufData(2);
					} else pointerBuf++;
				}
			
			}
			
			if(ifDraw && Global.ifHrmFragmentAlive){
				int[] multiValue = new int[8] ;
				try {
					for(int i=0;i<8;i=i+2){
						int index = i*5/2;
					index++;
					multiValue[i] = notiValue[index++];
					multiValue[i] = multiValue[i] << 8;
					multiValue[i] += (notiValue[index++] & 0xFF);
					buffer_HR[pointer_HR] = multiValue[i];
					buffer_HR_list.add(multiValue[i] + 0.0);
					pointer_HR++;
					if (pointer_HR == buf_length - 1) {			
						pointer_HR = 0;
						showHeartBeat (); 
						showVTVF();
					}
					multiValue[i+1] = notiValue[index++];
					multiValue[i+1] = (multiValue[i+1] << 8);
					multiValue[i+1] += (notiValue[index++] & 0xFF);
					}
					Message msg = Message.obtain(null, 0);
					Bundle b = new Bundle();
					b.putIntArray("multiValue", multiValue);
					System.out.print(multiValue);
					msg.setData(b); 
					ActivityMessenger.send(msg);
				}catch (Exception e){
					e.printStackTrace();
				}
			}
			
		  }
	};

	String getHexString(byte[] bytes) {
		StringBuilder stBuilder = new StringBuilder();

		for (Byte b : bytes) {
			stBuilder.append(String.format("%02x", b));
		}

		return stBuilder.toString();
	}

	/**
	 * Save ECG data to file (Compressed Sensing)
	 * 
	 * @param byteArrayList: ECG data to save
	 */
	/*private void saveToFileCs(ArrayList<byte[]> byteArrayList) {
		Log.v(TAG, "saveToFileCs - Start");
		
		ifSavingToFile = true;
		csCounter++;
		for(byte[] data : byteArrayList){
			try {
		    	output.write(data);
			} catch (Exception e) {
				e.printStackTrace();
				toastMakeText("Error: saveToFileCs!");
				stopSavingFinal();
			}
		}
		ifSavingToFile = false;
		if(!Global.ifSaving) stopSaving();
		if(csCounter==Global.savingLength/1000/4){
			stopSaving();
			startSaving(iffirst);
		}



		Log.v(TAG, "saveToFileCs - End");
	}*/
	/**
	 * Calculate Heart Beat
	 * 
	 * @param i: Which buffer to calculate Heart Beat, i = 1 or 2
	 */
	// TODO: Not working
	private void showVTVF() {
		// TODO: Test
		int b = hr.getHR(buffer_HR);
		int bpm = 0;

        Log.d(TAG, "showVTVF, bpm from HR_FFT: " + b);
		new Thread(new Runnable() {
	    	public void run(){
	    		MADetect1 template = new MADetect1();
	    		List<Double> Sample_Seq = new ArrayList<Double>();
	    		List<Double> data = new ArrayList<Double>(); // first 8 second data
	    		int i = 0;
	    		for (double tmp : buffer_HR) {
	    			if (i == 2000) {
	    				break;
	    			}
	    			data.add(tmp);
	    			i++;
	    		}
	    		boolean flagTemp = template.run(data);

				//bpm = template.getHR();

	    		boolean VTVF = false;
	    		if (flagTemp){
	    			Sample_Seq = template.getSampleResult();
	    			AveCC avecc = new AveCC();
	    			avecc.run(Sample_Seq, data);
	    			VTVF = avecc.getResult();	
	    		}
	    		int msg = VTVF ? 1 : -1;
				handler.sendEmptyMessage(msg);
	    	}
		   }).start();
		//bpm = -1;
		//updateBeat(bpm);
		if(bpm < Global.lowBpm && ifHbNormal){
			showNotification();
			ifHbNormal = false;
		}
		if(bpm >= 60) ifHbNormal = true;
	}
	private void showHeartBeat() {
        Log.d(TAG, "showHeartBeat");
		int bpm = -1;		
		boolean result = hrd.begin(buffer_HR_list);
		if (!result){
			System.out.println("Detection failure");
		}else {
            int bpmFromMADetect =
			bpm = (int)hrd.getHR();
		}
		hrd.reset();
		
	
		updateBeat(bpm);
		if(bpm > 0 && bpm < Global.lowBpm && ifHbNormal){
			showNotification();
			ifHbNormal = false;
		}
		if(bpm >= 60) ifHbNormal = true;
		buffer_HR_list.clear();
	}
	
	Handler handler = new Handler() {  
        @Override  
        public void handleMessage(Message msg) {
        	upVTVF(msg.what); 

        }  
    };
	/**
	 * Show Heart Beat on screen and widget
	 * 
	 * @param bpm: Heart Beat to show in integer
	 */
	private void updateBeat(int bpm) {
		try {
			Message msg = Message.obtain(null, 5);
			Bundle b = new Bundle();
			b.putInt("bpm", bpm);
			msg.setData(b);
			ActivityMessenger.send(msg);
		} catch (Exception e) {
			e.printStackTrace();
		}
		//Update widget
	    sendBroadcast(new Intent(Global.WidgetAction).putExtra("bpm", bpm));
	}
	
	private void upVTVF(int VTVF) {
		try {
			Message msg = Message.obtain(null, 7);
			Bundle b = new Bundle();
			b.putInt("VTVF", VTVF);
			msg.setData(b);
			ActivityMessenger.send(msg);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	//Show notification when low Heart Beat, and send SMS
	private void showNotification() {
		Notification noti = new Notification.Builder(BleService.this)
	        .setContentIntent(PendingIntent.getActivity
	        		(BleService.this, 0, Global.defaultIntent(BleService.this), 0))
            .setContentTitle(getResources().getString(R.string.app_name))
            .setContentText("Warning: Low heart beat!")
            .setSmallIcon(R.drawable.warning_64)
            .setAutoCancel(true)
            .setLights(Global.color_Red, 2000, 1000)
            .build();
		mNotiManager.notify(0, noti);
		vibrator.vibrate(3000);
		if(Global.ifRegUser && Global.ifSendSms){
			if(Global.ifAppendLoc && MainActivity.ifLCConnected){
				new Thread(){
					public void run(){
						Global.sendSMS(Global.emergencynum, Global.emergencymes +
								" My current location: ");
					}
				}.start();
			}else{
				Global.sendSMS(Global.emergencynum, Global.emergencymes);
			}
		}
	}
	/**
	 * Save ECG data from buffer to file
	 * 
	 * @param buf: Which buffer to save, buf = 1 or 2
	 */
	private void saveBufData(final int buf) {
		Log.d(TAG, "saveBuffData for buffer: " + buf + "\n" +
			"buf_length: " + buf_length + " pointerBufInit: " + pointerBufInit);

		if(flagSaveBufInit){
			flagSaveBufInit = false;
			byte[] tempByte = new byte[buf_length - pointerBufInit];
			System.arraycopy(buf==1 ? buffer_1 : buffer_2,
					pointerBufInit, tempByte, 0, buf_length - pointerBufInit);
			saveToFile(tempByte);
		}else{
			saveToFile(buf==1 ? buffer_1 : buffer_2);
		}
	}
	/**
	 * Save ECG data to file
	 * 
	 * @param data: ECG data to save in byte array
	 */
	private void saveToFile(final byte[] data) {
		Log.d(TAG, "saveToFile");
		try {
	    	output.write(data);
		} catch (Exception e) {
			e.printStackTrace();
			toastMakeText("Error: saveToFile!");
			stopSavingFinal(true);
		}
	}
	//Initiate for Notification receiving
	private void initNoti() {
		characteristic = mBluetoothGatt
				.getService(UUID.fromString("0000180d-0000-1000-8000-00805f9b34fb"))
				.getCharacteristic(UUID.fromString("00002a37-0000-1000-8000-00805f9b34fb"));
		descriptor = characteristic.getDescriptor
				(UUID.fromString("00002902-0000-1000-8000-00805f9b34fb"));
		descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
	}
	//Enable or disable Notification
	private void setNoti() {
		toastMakeText(String.valueOf(enableNoti));
		mBluetoothGatt.setCharacteristicNotification(characteristic, enableNoti);
		mBluetoothGatt.writeDescriptor(descriptor);
		if(Global.ifSaving && !enableNoti){
			stopSavingFinal(true);
		}
		enableNoti = !enableNoti;
		csByteArray.clear();
	}
	//Start Timer for saving ECG data to file
	private void startTimerSavingFile() {
		if(timerSavingFile == null){
			timerSavingFile = new Timer();
		}
		if(taskTimerSavingFile == null){
			taskTimerSavingFile = new TimerTask(){
				public void run(){
					if(flagTimerSavingFileFirst){
						flagTimerSavingFileFirst = false;
						return;
					}
					Message msg = Message.obtain();
					 //msg.what = iffirst;
					 handlerTimer.sendEmptyMessage(0);
				}
			};
		}
		if((timerSavingFile != null)&&(taskTimerSavingFile != null)){
			timerSavingFile.scheduleAtFixedRate(taskTimerSavingFile, 0, Global.savingLength);
		}
	}
	//Start Timer for saving ECG data to file
	private void cancelTimerSavingFile() {
		flagTimerSavingFileFirst = true;
		if(timerSavingFile != null){
			timerSavingFile.cancel();
			timerSavingFile = null;
		}
		if(taskTimerSavingFile != null){
			taskTimerSavingFile.cancel();
			taskTimerSavingFile = null;
		}
	}
	//Handler which stops previous saving and starts a new one
	private Handler handlerTimer = new Handler(){
		public void handleMessage(Message msg){
			stopSaving(true);
			startSaving(false);
		}
	};
	//Use mHandler to make toast text asynchronously
	private final void toastMakeText(final String string){
		mHandler.post(new Runnable() {
			public void run() {
				Global.toastMakeText(BleService.this, string);
			}
		});
	}
	//Life cycle
	public void onDestroy() {
		Log.i(TAG, "onDestroy()");
		
		//Update widget
	    sendBroadcast(new Intent(Global.WidgetAction).putExtra("bpm", 0));
        unregisterReceiver(wifiReceiver);
        stopSavingFinal(true);
        if(ConState != ConState_NotConnected) disconnect();
        if(mBluetoothGatt != null){
        	mBluetoothGatt.close();
        	mBluetoothGatt = null;
        }
		mNotiManager.cancelAll();
	}
}