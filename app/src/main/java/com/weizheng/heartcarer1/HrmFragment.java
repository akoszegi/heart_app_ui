package com.weizheng.heartcarer1;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import com.weizheng.heartcarer1.R;
import com.weizheng.ECG.ECGDetect;
import com.weizheng.ECG.MADetect;
import com.weizheng.heartcarer1.BleDeviceFinder.BleDeviceFinderInterface;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

/**
 * This Fragment behaves as a Heart Rate Monitor
 */
public class HrmFragment extends Fragment implements BleDeviceFinderInterface {
	private final String TAG = "HrmFragment";
	private static final int QUICK_TEST_TIME_IN_SEC = 30;
	public static boolean ifAutoConnect = false;
	private View view;
	private Button button_startTest;
	private ImageView hrImage;
    private Spinner spinner_channel;
	private MenuItem bleConnectionMenuItem;
	private TextView textview_heartRate, textview_vtvf;
	//private ToggleButton tButton_sensor, tButton_plot, tButton_save;
	//private TableRow tableRow1;
    private LinearLayout ecgButtonsLinearLayout;
	private String SelectFirst;
	private sendVoidToSMListener mCallback;
	//AChartEngine
	private TimeCount time;
	private String filename;
	private boolean flg = true;
	private ProgressDialog proDialog;

    /** Data Structures
     */
    //Data structure for data analyst
    class data_result {
        boolean isNoise;
        double HR;
        double QRS;
        double QTc;
        double PR;
        double ST;
        String time;
        public data_result(boolean isNoise, double HR, double QRS, double QTc, double PR, double ST) {
            this.isNoise = isNoise;
            this.HR = HR;
            this.QRS = QRS;
            this.QTc = QTc;
            this.PR = PR;
            this.ST = ST;
            this.time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
        }
    }

    /** Event Handling
     *
     *
     */
	//This interface is implemented by MainActivity, and HrmFragment can send Message to BleService
	public interface sendVoidToSMListener {
        public void sendVoidToSM(int num);
    }

    // BleDeviceFinderInterface implementation


	@Override
	public void deviceFound() {
		Log.d(TAG, "device Found");
		// TODO: use enum value
		mCallback.sendVoidToSM(1);
	}

	@Override
	public void scanFinished(boolean deviceFound) {
		Log.d(TAG, "scanFinished");
	}

	// Inner classes for event handling
    // TODO: implement channel selection
    private class SpinnerListener implements AdapterView.OnItemSelectedListener {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            switch (position) {
                // Channel 1 selected
                case 0:
                    Log.d(TAG, "Channel 1 selected");
                    break;
                // Channel 2 selected
                case 1:
                    Log.d(TAG, "Channel 2 selected");
                    break;
                // Both channels selected
                case 2:
                    Log.d(TAG, "Both selected");
                    break;

            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {}
    }

    class TimeCount extends CountDownTimer {
        public TimeCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }
        @Override
        public void onFinish() {
            button_startTest.setText("Start Test");
            button_startTest.setClickable(true);
            mCallback.sendVoidToSM(5);
            flg = true;
        }
        @Override
        public void onTick(long millisUntilFinished){
            button_startTest.setText(millisUntilFinished /1000+" seconds");
            if (millisUntilFinished / 1000 == 1)
                proDialog = ProgressDialog.show
                        (getActivity(), "Analysising", "", true, false);
            Global.quick_testing = true;
            if (flg) {
                button_startTest.setClickable(false);
                mCallback.sendVoidToSM(5);
                flg = false;
            }
        }
    }


	//Called when a fragment is first attached to its activity
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		Log.i(TAG, "onAttach()");
		mCallback = (sendVoidToSMListener) activity;
	}
	
	//Called to have the fragment instantiate its user interface view
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
		Log.i(TAG, "onCreateView()");	
		view = inflater.inflate(R.layout.hrm_fragment, container, false);
		setHasOptionsMenu(true);
		SelectFirst = "Select a " + getResources().getString(R.string.global_sensor) + " first!";

        uiSetup();
		initChart();
		if(Global.ifLandscape(getActivity())){
			adaptScreen(0);
		}
		return view;
	}


	private void uiSetup() {
        // Link local references to UI components
        textview_heartRate = (TextView)view.findViewById(R.id.textView_HR);
        textview_vtvf = (TextView)view.findViewById(R.id.textView_vtvf);
		bleConnectionMenuItem = (MenuItem) view.findViewById(R.id.ble_status);
        //tableRow1 = (TableRow)view.findViewById(R.id.tableRow1);
        ecgButtonsLinearLayout = (LinearLayout)view.findViewById(R.id.ecg_test_buttons_linear_layout);
        button_startTest = (Button)view.findViewById(R.id.start_test_button);
        spinner_channel = (Spinner)view.findViewById(R.id.channel_spinner);
		hrImage = (ImageView)view.findViewById(R.id.hr_image);

        // Start Button
        button_startTest.setOnClickListener(start_test_Listener);

        // Channel Selection Spinner
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.channels_array, R.layout.channel_spinner_item);
        adapter.setDropDownViewResource(R.layout.channel_spinner_dropdown_item);
        spinner_channel.setAdapter(adapter);
        spinner_channel.setOnItemSelectedListener(new SpinnerListener());
    }

	//Start test
	private OnClickListener start_test_Listener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			if (!Global.ifRegUser) {
                Toast.makeText(getActivity(),  "Only for registered User", Toast.LENGTH_SHORT).show();
                return;
			}
			if(BleService.mDevice==null || BleService.ConState!= 1){
	   			Global.toastMakeText(getActivity(), SelectFirst);
	   			return;
	    	}
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
	        final String[] selection = new String[2];
	        selection[0] = QUICK_TEST_TIME_IN_SEC + " Seconds Quick Test";
	        selection[1] = "Long Term Monitor";
	        builder.setItems(selection, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					if (which == 0) {
                        // Start 30 second timer - count down in 1 sec intervals
				        time = new TimeCount(QUICK_TEST_TIME_IN_SEC * 1000, 1000);
						time.start();
					} else {
						if (!Global.ifRegUser) {
				   			Global.toastMakeText(getActivity(), "Need to regist");
				   			return;
						}
						// TODO: Replace 4 with enum value
						mCallback.sendVoidToSM(4);
			   			Global.toastMakeText(getActivity(), "Start saving");
			   			button_startTest.setText("Long term testing");
			   			button_startTest.setClickable(false);
					}
				}
	        	
	        });
	        builder.show();
		}
		
	};


	//Connect with Heartrate Sensor
	private OnClickListener connectListener = new OnClickListener(){
    	public void onClick(View v){
            // Don't disconnect because we're in the middle of a quick test
    		if (Global.quick_testing) {
    			Global.toastMakeText(getActivity(), "Quick testing");
    			return;
    		}
    		if(BleService.mDevice==null){
    			Global.toastMakeText(getActivity(), SelectFirst);
    			return;
    		}
    		if(BleService.ConState == BleService.ConState_Connected){
    			//Disconnect
    			mCallback.sendVoidToSM(2);
    			return;
    		}
    		// TODO: I don't think this is ever called for the connect button - check
            // QUES: is this ever called?
    		if(!MainActivity.mBluetoothAdapter.isEnabled()){
    			startActivityForResult(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE), 1);
    		}else{
    			connectBle();
    		}
    	}
    };
    //Write value to Heartrate Sensor to enable/disable notification
    /*private OnClickListener writeCharValueListener = new OnClickListener(){
    	public void onClick(View v){
    		if(BleService.ConState != BleService.ConState_Connected){
    			Global.toastMakeText(getActivity(), getResources().getString(R.string.hrm_notincon));
    			tButton_sensor.setChecked(true);
    		}else{
    			mCallback.sendVoidToSM(3);
    		}
    	}
    };*/
    /*
    //Start or stop saving to file
	private OnClickListener saveToFileListener = new OnClickListener(){
    	public void onClick(View v){
    		if(!Global.ifRegUser){
    			Global.toastMakeText(getActivity(), getResources().getString(R.string.avail));
    			tButton_save.setChecked(false);
    			return;
    		}
    		mCallback.sendVoidToSM(4);
    	}
    };
    */
    /*
    //Start or stop drawing ECG signal
    private OnClickListener stopDrawingListener = new OnClickListener(){
    	public void onClick(View v){
    		if(Global.ifCsMode){
    			tButton_plot.setChecked(false);
    			Global.toastMakeText(getActivity(), "Not supported in CS mode!");
    			return;
    		}
    		BleService.ifDraw = !BleService.ifDraw;
    	}
    };
    */
    //Connect to sensor
    // TODO: Change 1 to enum value (Message_Types)
	private void connectBle() {
		//BleService.ConState = BleService.ConState_Connecting;

		String[] devToConnectTo = {getResources().getString(R.string.app_name_title)};
		UUID[] uuidsToConnectTo = {BleUuidLookup.HeartRateServiceUUID};

		BleDeviceFinder finder = new BleDeviceFinder(getActivity(), this);
		finder.findDevices(uuidsToConnectTo, devToConnectTo);

		//mCallback.sendVoidToSM(1);
	}

	private void disconnectBle() {
		mCallback.sendVoidToSM(2);
	}

    //Initiate chart
	private void initChart() {
		Global.Channel_selection=0;
        {
        	SingleChartFragment SingleChartFragment= new SingleChartFragment();
        	getFragmentManager().beginTransaction().replace
        	(R.id.chart, SingleChartFragment,getResources().
				getString(R.string.Single_Chart_Fragment)).commit();
        }

	}
	//Called by the system when the device configuration changes
	// TODO: Change this when new layout is finished
	@Override
    public void onConfigurationChanged(Configuration newConfig){
    	super.onConfigurationChanged(newConfig);
    	if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
    		adaptScreen(0);
    	}else{
    		adaptScreen(1);
    	}
    }
	//Hide views in table row when in landscape orientation
	// TODO: change when new layout finished
    private void adaptScreen(int i) {
    	if(i==0){
    		Log.v(TAG, "ORIENTATION_LANDSCAPE");
            ecgButtonsLinearLayout.setVisibility(View.GONE);
    		//tableRow1.setVisibility(View.GONE);
    		//tableRow2.setVisibility(View.GONE);
    	}else{
    		Log.v(TAG, "ORIENTATION_PORTRAIT");
            ecgButtonsLinearLayout.setVisibility(View.VISIBLE);
    		//tableRow1.setVisibility(View.VISIBLE);
    		//tableRow2.setVisibility(View.VISIBLE);
    	}
	}
    //Create menu
	// QUES: Test options menu when recording quick and long term tests
	public void onCreateOptionsMenu (Menu menu, MenuInflater inflater){
        Log.d(TAG, "onCreateOptionsMenu");
		inflater.inflate(R.menu.hrmfragment, menu);
		bleConnectionMenuItem = (MenuItem)menu.findItem(R.id.ble_status);

		// Start scanning for device right away
		connectBle();
	}

	//Handle menu item selected
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	       /* case R.id.cleargraph:
	        	clearGraph();
                return true;*/
	     /*   case R.id.selectsensor:
	        	selectSensor();
	            return true;
	        case R.id.sensorinfo:
	        	sensorInfo();
	            return true;
	        case R.id.selectchannel:
	        	selectChannel();
	            return true;    */
            case R.id.ble_status:
            	Log.d(TAG, "ble status tapped - conState = " + BleService.ConState);

                if (BleService.ConState == BleService.ConState_NotConnected) {
					Log.d(TAG, "need to connect to Bluetooth");
					//item.setIcon(getResources().getDrawable(R.drawable.pulse_heart));
					//hrImage.setBackground(getResources().getDrawable(R.drawable.heartrate_64));
					// TODO: test!!!
					if(!MainActivity.mBluetoothAdapter.isEnabled()) {
						startActivityForResult(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE), 0);
					}
					connectBle();
				} else {
					Log.d(TAG, "disconnect from bluetooth");
					//item.setIcon(getResources().getDrawable(R.drawable.heart_empty));
					//hrImage.setBackground(getResources().getDrawable(R.drawable.heart_empty));
					// TODO: Disconnect from device\
                    if (BleService.ConState == BleService.ConState_Connected) {
                        disconnectBle();
                    }



				}
				return true;
	    }
	    return false;
	}
	//Show sensor information
	private void sensorInfo() {
		if(BleService.mDevice==null){
			Global.toastMakeText(getActivity(), SelectFirst);
		}else{
    		String deviceName = (BleService.mDevice.getName() == null) ? "Unknown Device" : BleService.mDevice.getName();
    		Global.infoDialog(getActivity(),
    				getResources().getString(R.string.global_sensor) + getResources().getString(R.string.hrm_info), 
    				R.drawable.bluetooth_64, 
    				"Device Name: " + deviceName + "\nMac Address: " + BleService.mDevice.getAddress());
		}
	}
	//Show my device information
	/*private void myDevice() {
		String deviceName = MainActivity.mBluetoothAdapter.getName()==null ? 
				"NULL" : MainActivity.mBluetoothAdapter.getName();
		Global.infoDialog(getActivity(), getResources().getString(R.string.hrm_menu_my), 
				R.drawable.bluetooth_64, 
				getResources().getString(R.string.hrm_devicename) + ": " + deviceName + "\n" + 
				getResources().getString(R.string.hrm_mac) + ": " + MainActivity.mBluetoothAdapter.getAddress());
	}*/
	private void selectChannel(){
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Select a Channel");
        //int i= Global.Channel==2 ? 3:1;
        final String[] channel = new String[3];
        //if (Global.Channel==2){
        channel[0] ="Channel 1";
        channel[1] ="Channel 2";
        channel[2] ="Both Channel";
        //else{ 
        //channel[0] ="Channel "+String.valueOf(Global.Channel+1);
        //}
        builder.setItems(channel, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
            	if (Global.Channel_selection==which){}
            	else {
            		if (which==2){   //from 0 or 1 to 2
            			Global.Channel_selection = 2;
            		 	getFragmentManager().beginTransaction().replace
            			(R.id.chart, new DoubleChartFragment(),getResources().getString(R.string.Double_Chart_Fragment)).commit();
            	        }
            		else if(Global.Channel_selection!=2){  //from 1 to 0 or 0 to 1
            			Global.Channel_selection=which;
            			Message msg = Message.obtain(null, 1);
            			Bundle b = new Bundle();
            			b.putInt("which", which);	// QUES: Change to "channel" for clarity??
            			msg.setData(b);
            			SingleChartFragment SingleChartFragment = (SingleChartFragment)getFragmentManager().findFragmentByTag(
            	    			getResources().getString(R.string.Single_Chart_Fragment));
            			if(SingleChartFragment != null) SingleChartFragment.handleHrmFragmentMes(msg);
            		}
            		else{  //from 2 to 1 or 0
            			Global.Channel_selection=which;
            			 getFragmentManager().beginTransaction().replace
             			(R.id.chart, new SingleChartFragment(),getResources().getString(R.string.Single_Chart_Fragment)).commit();
            		}
            	};
                Toast.makeText(getActivity(),  channel[which], Toast.LENGTH_SHORT).show();
            }
        });
        builder.show();
    }
	//Select a Heartrate Sensor
	private void selectSensor() {
		if(BleService.ConState != BleService.ConState_NotConnected){
			Global.toastMakeText(getActivity(), getResources().getString(R.string.hrm_disfirst));
			return;
		}
		if(!MainActivity.mBluetoothAdapter.isEnabled()) {
			startActivityForResult(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE), 0);
		} else {
			//startDevicePicker();
		}
	}
	//Handle result from enable-bluetooth Intent
	public void onActivityResult(int requestCode, int resultCode, Intent data){
		if(requestCode==0){
			if(resultCode==Activity.RESULT_OK)
				startDevicePicker();
			else
				Global.toastMakeText(getActivity(), getResources().getString(R.string.hrm_turnon));
		}else if(requestCode==1){
			if(resultCode==Activity.RESULT_OK)
				connectBle();
			else
				Global.toastMakeText(getActivity(), getResources().getString(R.string.hrm_turnon));
		}
	}
	//Start BleDeviceFinder Activity to select a Heartrate Sensor
	private void startDevicePicker() {
		//startActivity(new Intent(getActivity(), BleDeviceFinder.class));
	}
	//Show test result
	
	
	private void show_result() {
		new Thread(new Runnable() {
	    	public void run(){
	    		data_result result = data_analysis(filename);
	    		if (result.isNoise) {
	    			getActivity().runOnUiThread(new Runnable() {
	    				public void run() {
	    					Dialog alertDialog = new AlertDialog.Builder(getActivity()).
	    							setTitle("Wrong Data").
	    							setMessage("The Data is too noisy to analyse" + filename).
	    							setIcon(R.drawable.report).
	    							setPositiveButton("Return", new DialogInterface.OnClickListener() {  
	    								@Override
	    								public void onClick(DialogInterface dialog, int which) {
	    								}
	    							}).create();
	    					alertDialog.show();
	    					handler.sendEmptyMessage(0);
	    				}
	    			});
	    		} else {
	    			File file = new File(filename);
	    			file.delete();
	    			Intent intent = new Intent(getActivity(), QuickResult.class);
	    			intent.putExtra("isNoise", result.isNoise);
	    			intent.putExtra("HR", String.valueOf((int)result.HR));
	    			intent.putExtra("QRS", String.valueOf((int)result.QRS));
	    			intent.putExtra("QTC", String.valueOf((int)result.QTc));
	    			intent.putExtra("PR", String.valueOf((int)result.PR));
	    			intent.putExtra("ST", String.valueOf((int)result.ST));
	    			intent.putExtra("time", result.time);
	    			intent.putExtra("filename", filename);
	    			startActivity(intent);	
	    			proDialog.dismiss();
	    		}
	    	}
	    }).start();
	}
	Handler handler = new Handler() {  
        @Override  
        public void handleMessage(Message msg) {
        	proDialog.dismiss(); 
        }  
    };  
	
	

	
	/**
	 * 
	 * @param filename : quick check's file name
	 * @return analysis result
	 */
	protected data_result data_analysis(String filename) {
		MADetect ecg = new MADetect();
		ecg.setFS(250);
		//ecg.setPath(Global.quickcheckpath + "/" + "201603171356070201603171356071.bin");
		ecg.setPath(Global.quickcheckpath + "/" + filename);
		ecg.run();
		List<Integer> MA_Seq = new ArrayList<Integer>();
		List<Double> New_ECG = new ArrayList<Double>();
		MA_Seq = ecg.getMASeq();
		New_ECG = ecg.getNewECG();
		for (int index = 0; index < MA_Seq.size(); ++index){
			System.out.println(MA_Seq.get(index));
		}

		// TODO: Heart rate calculated in MADetect - not right
		double HR = ecg.getHR();
		ECGDetect ecg2 = new ECGDetect();
		ecg2.setFS(250);
		if (!ecg2.run(New_ECG)) {
			return new data_result(true, 0.0, 0.0, 0.0, 0.0, 0.0);
		}
		// TODO: Interval values calculated in ECGDetect not right
		double QRS = ecg2.getQRS_duration();
		double QTc = ecg2.getQTc();
		double PR = ecg2.getPR_interval();
		double ST_avg = ecg2.getSTavg();
		return new data_result(false, HR, QRS, QTc, PR, ST_avg);
	}

	/**
	 * Plot point on chart
	 *
	 */
	//Handle Message from MainActivity, originally from BleService
	public void handleMainActivityMes(Message msg){
		int i = msg.what;
		// TODO: Add Message_Types enum value for i
		if(i==0){
			//plotPoint(msg.getData().getByte("tempValue"));
			if (Global.Channel_selection!=2){
			getFragmentManager().executePendingTransactions();
	    	SingleChartFragment SingleChartFragment = (SingleChartFragment)getFragmentManager().findFragmentByTag(
	    			getResources().getString(R.string.Single_Chart_Fragment));
			if(SingleChartFragment != null) SingleChartFragment.handleHrmFragmentMes(msg);}
			else{
			DoubleChartFragment DoubleChartFragment = (DoubleChartFragment)getFragmentManager().findFragmentByTag(
					getResources().getString(R.string.Double_Chart_Fragment));
			if(DoubleChartFragment != null) DoubleChartFragment.handleHrmFragmentMes(msg);
			}
		}else if(i==1){
			//Connected
            Log.d(TAG, "Ble connected");
			Global.toastMakeText(getActivity(), getResources().getString(R.string.global_sensor) + " connected!");
			bleConnectionMenuItem.setIcon(getResources().getDrawable(R.drawable.bluetooth_blue));
			hrImage.setBackground(getResources().getDrawable(R.drawable.heartrate_64));

		}else if(i==2){
			//Disconnected
            Log.d(TAG, "Ble disconnected");
			// Reset UI
			initViews();
			//bleConnectionMenuItem.setIcon(R.drawable.bluetooth_gray);
			Global.toastMakeText(getActivity(), "Disconnected!");

			if (Global.Channel_selection!=2){
		    	SingleChartFragment SingleChartFragment = (SingleChartFragment)getFragmentManager().findFragmentByTag(
		    			getResources().getString(R.string.Single_Chart_Fragment));
			if(SingleChartFragment != null) SingleChartFragment.handleHrmFragmentMes(msg);}
			else{
				DoubleChartFragment DoubleChartFragment = (DoubleChartFragment)getFragmentManager().findFragmentByTag(
						getResources().getString(R.string.Double_Chart_Fragment));
			if(DoubleChartFragment != null) DoubleChartFragment.handleHrmFragmentMes(msg);
				}
			button_startTest.setText("Start Test");
   			button_startTest.setClickable(true);
		}else if(i==3){
			//Start saving
			//tButton_save.setChecked(true);
			filename = msg.getData().getString("filename");
		}else if(i==4){
			//Stop saving
			Log.d(TAG, "tButton_save.setChecked(false);");
			if (Global.quick_testing){
				show_result();
				Global.quick_testing = false;
			}
			//tButton_save.setChecked(false);
		}else if(i==5){
			//updateBeat
			textview_heartRate.setText(String.valueOf(msg.getData().getInt("bpm"))+" bpm");
		}else if(i==6){
			initViews();
		}else if(i == 7) {
			int tmp = msg.getData().getInt("VTVF");
			if (tmp == -1)
				textview_vtvf.setText("");
			else 
				textview_vtvf.setText("VT/VF");
		}
	}

    //Initiate Views
    private void initViews() {
        textview_heartRate.setText("0 bmp");
        textview_vtvf.setText("");
		hrImage.setBackground(getResources().getDrawable(R.drawable.heart_empty));

		if (bleConnectionMenuItem != null && BleService.ConState == BleService.ConState_NotConnected) {
			bleConnectionMenuItem.setIcon(R.drawable.bluetooth_gray);
		}


		//tButton_sensor.setChecked(BleService.ConState==BleService.ConState_Connected ? !BleService.enableNoti : BleService.enableNoti);
        //tButton_plot.setChecked(BleService.ifDraw);
        //tButton_save.setChecked(Global.ifSaving);
    }

	//Life cycle
	public void onResume (){
    	super.onResume();
    	Log.i(TAG, "onResume()");
    	
		Global.ifHrmFragmentAlive = true;
    	//Initiate views
		initViews();
    	//Auto connect
    	if(ifAutoConnect){
    		ifAutoConnect = false;
    		mCallback.sendVoidToSM(1);
    	}
    }
    public void onPause (){
    	super.onPause();
    	Log.i(TAG, "onPause()");
    	
		Global.ifHrmFragmentAlive = false;
    }
	public void onDestroy (){
		super.onDestroy();
		Log.i(TAG, "onDestroy()");
	}
}