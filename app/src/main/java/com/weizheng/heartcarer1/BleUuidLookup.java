package com.weizheng.heartcarer1;

import java.util.HashMap;
import java.util.UUID;

/** Bluetooth Service and Characteristic UUIDs
 * *** The app and board use the Bluetooth defined Heart Rate Service
 *
 * Created by AlexKoszegi on 2017-07-17.
 */

public class BleUuidLookup {
    private static HashMap<String, String> attributes = new HashMap();

   // public static String HeartRateServiceUUID = "0000180d-0000-1000-8000-00805f9b34fb";

    public static UUID HeartRateServiceUUID = UUID.fromString("0000180d-0000-1000-8000-00805f9b34fb");

    static {
        //  Services.
        attributes.put("0000180d-0000-1000-8000-00805f9b34fb", "Heart Rate Service");
        attributes.put("0000180a-0000-1000-8000-00805f9b34fb", "Device Information Service");

        // Characteristics.
        attributes.put("00002a37-0000-1000-8000-00805f9b34fb", "Heart Rate Measurement");
        attributes.put("00002a29-0000-1000-8000-00805f9b34fb", "Manufacturer Name String");
    }

    public static String lookup(String uuid, String defaultName) {
        String name = attributes.get(uuid);
        return name == null ? defaultName : name;
    }

    public static UUID getUUID(String uuid, String defaultName) {
        return UUID.fromString(lookup(uuid, defaultName));
    }
}
