package com.weizheng.heartcarer1;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import  android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.weizheng.database.FeedReaderContract;
import com.weizheng.database.FeedReaderDbHelper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by AlexKoszegi on 2017-07-19.
 */

public class QuickTestsTabContent extends Fragment {
    String TAG = "QuickTestsTabContent";
    ListView quickTestsList;

    ArrayList<DataResult> data;

    private Cursor c;
    private FeedReaderDbHelper mDbHelper;
    private SQLiteDatabase db;
    private int cursorIndex = 0;
    private int nameColIndex;
    private int hrColIndex;
    private int qrsColIndex;
    private int prColIndex;
    private int qtcColIndex;
    private int stColIndex;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");

        // UI Setup
        View v = inflater.inflate(R.layout.quick_tests_tab_content, container, false);

        quickTestsList = (ListView) v.findViewById(R.id.quick_test_list);

        // Database
        dataBaseAccessSetUp();
        //getDatabaseContent();
        getContent1();



        return v;
    }

    private void dataBaseAccessSetUp() {
        // Database Access
        mDbHelper = new FeedReaderDbHelper(getActivity().getBaseContext());
        db = mDbHelper.getWritableDatabase();

        // SQL Query
        c = db.query(FeedReaderContract.FeedEntry.TABLE_NAME,
                new String[]{"HR", "QRS" , "QTC", "PR", "ST", "TestTime"},
                FeedReaderContract.FeedEntry.COLUMN_NAME_UserID  + "= ?",
                new String[]{String.valueOf(Global.userid)},
                null,
                null,
                FeedReaderContract.FeedEntry.COLUMN_NAME_TestTime + " desc");

        Log.d("QuickTestsTabContent", "c db.query count: " + c.getCount()
                + "\n column count: "+ c.getColumnCount() +
        "\n" + "column strings: " + c.getColumnNames().toString());

        // Index set up
        nameColIndex = c.getColumnIndex(FeedReaderContract.FeedEntry.COLUMN_NAME_TestTime);

        hrColIndex = c.getColumnIndex(FeedReaderContract.FeedEntry.COLUMN_NAME_HR);
        qrsColIndex = c.getColumnIndex(FeedReaderContract.FeedEntry.COLUMN_NAME_QRS);
        qtcColIndex = c.getColumnIndex(FeedReaderContract.FeedEntry.COLUMN_NAME_QTC);
        prColIndex = c.getColumnIndex(FeedReaderContract.FeedEntry.COLUMN_NAME_PR);
        stColIndex = c.getColumnIndex(FeedReaderContract.FeedEntry.COLUMN_NAME_ST);


    }

    private void getDatabaseContent() {
        Log.d(TAG, "getDatabaseContent");

        while (c.moveToNext() ) {
            Log.d(TAG, "get db content - count: " + c.getCount());

            String name = (String) c.getString(nameColIndex);
            int hr = (int) c.getInt(hrColIndex);
            int qrs = (int) c.getInt(qrsColIndex);
            int pr = (int) c.getInt(prColIndex);
            int qtc = (int) c.getInt(qtcColIndex);
            int st = (int) c.getInt(stColIndex);
            Log.d(TAG, "values: name: " + name + "\n hr: " + hr +
                "\n qrs: " + qrs + "\n pr: " + pr + "\n qtc: " + qtc +
                "\n st: " + st);

            if (c.getCount() != 0) {

            }

        }
    }

    // How to use cursor?
    // No data is being returned from c
    // But in mDbHelper -> mName -> count = 13
        // That's the number of quick tests saved on my phone
    private void getContent1() {
        List<List<Date>> List_date = new ArrayList<List<Date>>();
        List<List<Integer>> List_value = new ArrayList<List<Integer>>();
        List<Date> date = new ArrayList<Date>();
        List<Integer> Value_list = new ArrayList<Integer>();
        String str = "HR";

        c.moveToFirst();
        c.moveToPrevious();
        int max = 0;
        int min = Integer.MAX_VALUE;
        while(c.moveToNext()){
            int nameColumnIndex = c.getColumnIndex(FeedReaderContract.FeedEntry.COLUMN_NAME_TestTime);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date D = null;
            try {
                D = sdf.parse(c.getString(nameColumnIndex).substring(0, 10));
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            date.add(D);
            nameColumnIndex = c.getColumnIndex(str);
            int int_tmp = c.getInt(nameColumnIndex);
            Value_list.add(int_tmp);
            min = Math.min(min, int_tmp);
            max = Math.max(max, int_tmp);
        }
        List_date.add(date);
        List_value.add(Value_list);
    }
}
